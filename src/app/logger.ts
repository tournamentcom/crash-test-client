import { Injectable } from '@angular/core';

export enum MessageType {
    Info = 'Info',
    Success = 'Success',
    Warning = 'Warning',
    Error = 'Error'
}

export interface Message {
    text: string;
    type: MessageType;
    data?: any;
    timestamp: Date;
}

@Injectable({ providedIn: 'root' })
export class Logger {
    private readonly $messages: Message[] = [];

    public get messages(): Message[] {
        return this.$messages;
    }

    public info(text: string, data?: any): void {
        this.log(text, MessageType.Info, data);
    }

    public success(text: string, data?: any): void {
        this.log(text, MessageType.Success, data);
    }

    public warn(text: string, data?: any): void {
        this.log(text, MessageType.Warning, data);
    }

    public error(text: string, data?: any): void {
        this.log(text, MessageType.Error, data);
    }

    public log(text: string, type: MessageType, data?: any): void {
        data = data ? JSON.parse(JSON.stringify(data)) : undefined;
        this.$messages.unshift({ text, type, data, timestamp: new Date() });
    }
}
