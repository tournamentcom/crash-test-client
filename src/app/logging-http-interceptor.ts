import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Logger } from './logger';

@Injectable()
export class LoggingHttpInterceptor implements HttpInterceptor {
    constructor(private readonly log: Logger) {
    }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.body)
            this.log.info(`Request: ${req.method} ${req.url}`, req.body);
        else
            this.log.info(`Request: ${req.method} ${req.url}`);

        return next.handle(req).pipe(
            tap(evt => {
                if (!(evt instanceof HttpResponse))
                    return;

                if (evt.body)
                    this.log.success(`Response: ${evt.status} ${req.url}`, evt.body);
                else
                    this.log.success(`Response: ${evt.status} ${req.url} - ${evt.statusText}`);
            }),
            catchError(err => {
                if (err instanceof HttpErrorResponse)
                    this.log.error(`Response: ${err.status} ${err.url}`, err);

                return throwError(err);
            })
        );
    }
}
