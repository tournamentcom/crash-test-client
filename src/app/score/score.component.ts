import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { filter } from 'rxjs/operators';
import { calculateScore } from '../score-calculator';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScoreComponent implements OnInit {
  public score = '1.00';
  public time = 0;

  @Input()
  public roundTime?: number;

  @Input()
  public live = true;

  constructor(
    private readonly changeDetector: ChangeDetectorRef) { }

  public ngOnInit(): void {
    interval(10).pipe(filter(() => this.live && !!this.roundTime)).subscribe(() => {
      if (!this.roundTime)
        return;

      this.score = (calculateScore(this.roundTime)).toFixed(2);
      this.changeDetector.detectChanges();
    });
  }

}
