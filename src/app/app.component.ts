import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { CrashService, GameData } from './crash.service';
import { Logger } from './logger';
import { environment } from '../environments/environment';
import { CrashMessage, MessageType, PlayerBailedMessage, WebsocketService } from './websocket.service';
import { interval } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CountdownEvent } from 'ngx-countdown';
import { calculateScore } from './score-calculator';

enum GameState {
  Initialising = 'Initialising',
  Joining = 'Joining',
  Waiting = 'Waiting',
  Running = 'Running',
  Crashed = 'Crashed',
  Ended = 'Ended',
  Error = 'Error'
}

interface LaunchParameters {
  operator: string;
  gameId: string;
  token: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private parameters!: LaunchParameters;
  private sessionToken!: string;
  public gameData!: GameData;
  private updating = true;
  public thisRoundStartTime?: number;
  public bailed = false;
  public ended = false;
  public lastScore = '1.00';
  public history: number[] = [];

  public state = GameState.Initialising;

  public bails: PlayerBailedMessage[] = [];

  constructor(
    private readonly service: CrashService,
    public readonly websocket: WebsocketService,
    public readonly log: Logger) {
  }

  public async ngOnInit(): Promise<void> {
    try {
      this.log.info('Initialising...');
      this.getLaunchParameters();
      this.log.info(`API Host: ${environment.apiHost}`);
      await this.auth();
      await this.join();
      this.websocket.connect(this.parameters.operator, this.parameters.gameId);
      this.websocket.message.pipe(
        filter(m => m.type === MessageType.Crash),
        map(event => event as CrashMessage)).subscribe(async msg => this.crash(msg));
      this.websocket.message.pipe(
        filter(m => m.type === MessageType.PlayerBailed),
        map(event => event as PlayerBailedMessage)).subscribe(m => this.bails.unshift(m));
      interval(5000)
        .pipe(filter(() => this.updating && [GameState.Waiting, GameState.Running].includes(this.state)))
        .subscribe(() => this.update());
    } catch (err) {
      this.log.error((err as Error).message);
      this.state = GameState.Error;
    }
  }

  private async auth(): Promise<void> {
    if (!this.parameters)
      throw new Error('Launch parameters not found.');

    try {
      this.log.info(`Authenticating game ${this.parameters.gameId}`);
      const response = await this.service.auth(this.parameters.operator, this.parameters.gameId, this.parameters.token);
      this.sessionToken = response.sessionToken;
      this.log.success(`Game authenticated.`);
    } catch (err) {
      this.log.error('Auth failed!');
      throw err;
    }
  }

  private async join(): Promise<void> {
    if (!this.parameters)
      throw new Error('Launch parameters not found.');

    this.state = GameState.Joining;

    try {
      this.log.info(`Joining game ${this.parameters.gameId}`);
      this.gameData = await this.service.join(this.parameters.operator, this.parameters.gameId, this.sessionToken);
      this.sessionToken = this.gameData.sessionToken;
      this.log.success(`Game joined.`);
      if (this.gameData.startTime < Date.now() && this.gameData.roundStartTime)
        if (this.gameData.crashTime && this.gameData.crashTime <= Date.now())
           this.state = GameState.Crashed;
        else {
          this.thisRoundStartTime = this.gameData.roundStartTime;
          this.state = GameState.Running;
        }
      else
        this.state = GameState.Waiting;
    } catch (err) {
      this.log.error('Join failed!');
      throw err;
    }
  }

  private async update(): Promise<void> {
    if (this.state === GameState.Ended)
      return;

    if (this.gameData.roundNumber === -1) {
      this.updating = false;
      return;
    }

    this.updating = false;
    try {
      const status = await this.service.status(
        this.parameters.operator,
        this.parameters.gameId,
        this.gameData.roundNumber,
        this.sessionToken);
      this.gameData.roundStartTime = status.roundStartTime;
      this.gameData.playerCount = status.playerCount;
      this.sessionToken = status.sessionToken;
      this.gameData.inPlay = status.inPlay;
    } finally {
      this.updating = true;
    }
  }

  public async onNextRoundCountdownEvent(e: CountdownEvent): Promise<void> {
    if (e.action !== 'done')
      return;

    if (this.ended) {
      this.state = GameState.Ended;
      return;
    }

    this.thisRoundStartTime = this.gameData.roundStartTime;
    this.bails = [];

    if (this.state === GameState.Crashed)
      this.gameData.roundNumber++;
    this.updating = true;
    await this.update();
    this.bailed = false;
    this.gameData.player.score = undefined;
    this.gameData.player.moveTime = undefined;
    this.gameData.remainingPlayerCount = this.gameData.playerCount;
    this.state = GameState.Running;
    this.gameData.crashTime = undefined;
  }

  public async onEndCountdownEvent(e: CountdownEvent): Promise<void> {
    if (e.action !== 'done')
    return;

    this.ended = true;
  }

  private getLaunchParameters(): void {
    const paramNames = ['operator', 'gameId', 'token'];
    const url = window.location.href;

    const httpParams = new HttpParams({ fromString: url.split('?')[1] });
    const missing: string[] = [];

    this.parameters = paramNames.reduce((params: any, key: string) => {
      const paramValue = httpParams.get(key);

      if (!paramValue) {
        missing.push(key);
        return params;
      }

      params[key] = paramValue;
      return params;
    }, {});

    if (missing.length === 0) {
      this.log.success(`Launch parameters`, this.parameters);
      return;
    }

    throw new Error(`Launch parameter(s) missing: ${missing.join(', ')}`);
  }

  private async crash(msg: CrashMessage): Promise<void> {
    if (this.state !== GameState.Running)
      return;

    this.state = GameState.Crashed;
    this.gameData.crashTime = msg.crashTime;
    this.lastScore = this.calculateScore();
    this.gameData.roundStartTime = msg.roundStartTime;
  }

  public async bail(): Promise<void> {
    try {
      this.updating = true;
      const response = await this.service.play(this.parameters.operator, this.parameters.gameId, this.sessionToken);
      this.sessionToken = response.sessionToken;
      this.gameData.player.score = response.score;
      this.gameData.player.moveTime = Date.now();
      this.gameData.remainingPlayerCount = Math.max(this.gameData.remainingPlayerCount - 1, 0);
      this.bailed = true;
    } catch (err) {
      if (err instanceof HttpErrorResponse) {
        alert(err.error.message);
        return;
      }

      alert((err as Error).message);
    } finally {
      this.updating = false;
    }
  }

  public calculateHistoryScore(startTime: number): string {
    return (calculateScore(startTime, this.gameData.endTime)).toFixed(2);
  }

  public calculateScore(): string {
    if (!this.gameData.roundStartTime || !this.gameData.crashTime)
      return '1.00';

    return (calculateScore(this.gameData.roundStartTime, this.gameData.crashTime)).toFixed(2);
  }

  public canBail(): boolean {
    if (this.state !== GameState.Running)
      return false;

    return !!this.gameData.inPlay;
  }
}
