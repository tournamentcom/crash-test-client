import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

export interface JoinGameRequest {
}

export interface GameData {
  sessionToken: string;
  startTime: number;
  endTime?: number;
  prizeAmount?: number;
  currency: string;
  roundStartTime?: number;
  safeTime: number;
  alpha: number;
  beta: number;
  gamma: number;
  maxRounds?: number;
  roundNumber: number;
  timestamp: number;
  playerCount: number;
  remainingPlayerCount: number;
  crashTime?: number;
  inPlay?: boolean;
  player: {
    id: string;
    displayName: string;
    balance?: number;
    score?: number;
    moveTime?: number;
  };
}

export interface PlayResponse {
  balance: number;
  sessionToken: string;
  score: number;
}

export interface GameStatusResponse {
  sessionToken: string;
  playerCount: number;
  roundNumber: number;
  roundStartTime: number;
  timestamp: number;
  inPlay?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class CrashService {
  constructor(
    private readonly http: HttpClient) { }

  public async auth(operatorId: string, gameId: string, token: string): Promise<GameData> {
    const url = `https://${environment.apiHost}/game/${operatorId}/${gameId}/auth`;
    const request: JoinGameRequest = {
      token
    };
    return this.http.post<GameData>(url, request).toPromise();
  }

  public async join(operatorId: string, gameId: string, sessionToken: string): Promise<GameData> {
    const url = `https://${environment.apiHost}/game/${operatorId}/${gameId}/join`;
    const request: JoinGameRequest = {};
    return this.http.post<GameData>(url, request, {
      headers: {
        'session-token': sessionToken
      }
    }).toPromise();
  }

  public async status(operatorId: string, gameId: string, roundNumber: number, sessionToken: string): Promise<GameStatusResponse> {
    const url = `https://${environment.apiHost}/game/${operatorId}/${gameId}/${roundNumber}`;
    return this.http.get<GameStatusResponse>(url, {
      headers: {
        'session-token': sessionToken
      }
    }).toPromise();
  }

  public async play(operatorId: string, gameId: string, sessionToken: string): Promise<PlayResponse> {
    const url = `https://${environment.apiHost}/game/${operatorId}/${gameId}/play`;
    return this.http.post<PlayResponse>(url, {}, {
      headers: {
        'session-token': sessionToken
      }
    }).toPromise();
  }
}
