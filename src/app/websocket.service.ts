import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { Logger } from './logger';
import { io, Socket } from 'socket.io-client';

export enum MessageType {
  Crash = 'Crash',
  TimeSync = 'TimeSync',
  PlayerBailed = 'PlayerBailed'
}

interface MessageBase {
  type: MessageType;
  serverTime: number;
}

export interface CrashMessage extends MessageBase {
  type: MessageType.Crash;
  crashTime: number;
  roundStartTime: number;
  roundNumber: number;
}

export interface PlayerBailedMessage extends MessageBase {
  type: MessageType.PlayerBailed;
  displayName: string;
  score: number;
  moveTime: number;
  avatarUrl?: string;
}

export interface TimeSyncMessage extends MessageBase {
  type: MessageType.TimeSync;
}

export type Message = CrashMessage | PlayerBailedMessage | TimeSyncMessage;

export enum WebsocketState {
  Connecting = 'Connecting',
  Connected = 'Connected',
  Reconnecting = 'Reconnecting',
  Disconnected = 'Disconnected'
}

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  public state = WebsocketState.Disconnected;

  private socket?: Socket;
  private messageSubject: Subject<Message> = new Subject<Message>();

  public get message(): Observable<Message> {
    return this.messageSubject.asObservable();
  }

  constructor(
    private readonly log: Logger) {
  }

  public connect(operatorId: string, gameId: string): Promise<Observable<Message>> {
    return new Promise((resolve) => {
      const url = `https://${environment.apiHost}`;

      this.log.info('Connecting to websocket server...', {
        url
      });

      this.socket = io(url, {
        path: '/ws',
        query: {
          operator: operatorId,
          gameId
        }
      });

      this.state = WebsocketState.Connecting;

      this.socket.on('connect', () => {
        this.state = WebsocketState.Connected;
        this.log.success('Websocket connected.');
        resolve(this.message);
      });

      this.socket.on('reconnect_attempt', () => {
        this.state = WebsocketState.Reconnecting;
        this.log.warn('Websocket reconnecting.');
      });

      this.socket.on('reconnect_failed', () => {
        this.state = WebsocketState.Disconnected;
        this.log.warn('Websocket disconnected.');
      });

      this.socket.on('crash', (obj: any) => {
        if (obj.crashEvent === 'second')
          return;

        this.log.info('Crash', obj);

        this.messageSubject.next({
          type: MessageType.Crash,
          crashTime: obj.crashTime,
          roundStartTime: obj.roundStartTime,
          roundNumber: obj.roundNumber,
          serverTime: obj.serverTime
        });
      });

      this.socket.on('timeSync', (obj: any) => {
        this.log.info('Time Sync', obj);
        this.messageSubject.next({
          type: MessageType.TimeSync,
          serverTime: obj.serverTime
        });
      });

      this.socket.on('playerCrash', (obj: any) => {
        this.log.info('Player Bailed', obj);
        this.messageSubject.next({
          type: MessageType.PlayerBailed,
          displayName: obj.displayName,
          moveTime: obj.moveTime,
          score: obj.score,
          serverTime: obj.serverTime
        });
      });
    });
  }
}
