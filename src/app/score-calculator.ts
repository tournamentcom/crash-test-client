export function calculateScore(startTime: number, endTime?: number): number {
    const timeDiff = Math.max(((endTime || Date.now()) - startTime), 0);
    const r = 0.00006;
    const multiplier = Math.floor(100 * Math.pow(Math.E, r * timeDiff)) / 100;
    return multiplier >= 1 ? multiplier : 1;
}
